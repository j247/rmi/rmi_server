package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;


public class ImplementServer extends UnicastRemoteObject implements InterfaceServer
{
	private static ArrayList<Persons> listPersons()
	{
		ArrayList<Persons> list = new ArrayList<Persons>();
		
		list.add(new Persons(1, "Huber Arboleda", "haas@haas.com", "Docente", 5000));
		list.add(new Persons(2, "Jose Arboleda", "jose@haas.com", "Director", 25000));
		list.add(new Persons(3, "Lorena Arboleda", "loren@haas.com", "Cordinador", 10000));
		list.add(new Persons(4, "Claudia Arboleda", "clau@haas.com", "Docente", 5000));
		list.add(new Persons(5, "Rosa Arboleda", "rosa@haas.com", "Registro y control", 10000));
		list.add(new Persons(6, "Carlos Arboleda", "carlos@haas.com", "Docente", 5000));
		
		return list;
	}
	
	private static String getPersons(int id) 
	{
		
		return "Nombre: " + listPersons().get(id-1).getName()+"\n"
				+"Correo: " + listPersons().get(id-1).getEmail()+"\n"
				+"Cargo: " + listPersons().get(id-1).getJobTitle()+"\n"
				+"Sueldo: " + listPersons().get(id-1).getSalary();
	}
	
	public ImplementServer() throws RemoteException {}
	
	public String consult(int id) throws Exception 
	{
		
		if(id < listPersons().size()+1)
		{
			return getPersons(id);			
		}
		else 
		{
			return "No existe registro con el ID ingresado";
		}
	}
}
