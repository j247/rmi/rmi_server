package server;

import java.rmi.Remote;

public interface InterfaceServer extends Remote
{
	public String consult(int id) throws Exception;
}
