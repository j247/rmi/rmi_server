package server;

import java.rmi.Naming;
import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;

public class ProcessServer 
{
	public static void main(String[] args)  throws Exception
	{
		
		ImplementServer server = new ImplementServer();
		LocateRegistry.createRegistry(1099);
		String rmiObjectName = "rmi://localhost/Data";
		Naming.rebind(rmiObjectName, (Remote) server);
		System.out.println("(Server Run)");		

	}
}
