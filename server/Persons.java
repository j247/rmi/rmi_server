package server;

public class Persons 
{
	private int id;
	private String name;
	private String email;
	private String jobTitle;
	private double salary;
	
	public Persons(int id, String name, String email, String jobTitle, double salary) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.jobTitle = jobTitle;
		this.salary = salary;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public double getSalary() {
		return salary;
	}
	
	
	
	
}
